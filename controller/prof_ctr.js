const ProfModel= require('../modules/prof');
const bcrypt =require('bcryptjs');
const jwt =require('jsonwebtoken');
module.exports= {

    addProf: function (req, res) {
        const prof = new ProfModel({
            nom:req.body.nom,
            prenom:req.body.prenom,
            email:req.body.email,
            pwd: req.body.pwd,
           Tel: req.body.Tel,
        });
       prof.save(function (err) {
            if (err) {
                res.json({state: 'no', msg: 'vous avez un erreur'})
            } else {
                res.json({state: 'ok', msg: 'Prof ajouté'})
            }
        })
    },
    login:function(req,res){
      ProfModel.findOne({email:req.body.email}).populate('role').exec(function (err,userInfo) {
       if (err){
           res.json({state:'no',msg:'vous avez un erreur'})
       }else{
           if(bcrypt.compareSync(req.body.pwd,userInfo.pwd)){
               //Generation token
                    var token=jwt.sign({id:userInfo._id},req.app.get('secretKey'),
                        {
                        expiresIn: 86400,//expire in 24h

                    })

               res.json({state:'ok',msg:'user found',data:{user:userInfo,token:token}})
           }
           else{
               res.json({state:'no',msg:'password no valid'})
           }
       }
      })
    },
    getAll: function (req, res){
        ProfModel.find({}, function(err,liste){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(liste)
            }
        })
    },
    getById: function (req, res){
        ProfModel.findOne({_id:req.params.id},  function(err,liste){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(liste)
            }
        })
    },
    SupprimerProf: function (req, res){
        ProfModel.remove({_id:req.params.id}, function(err){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json({state :'ok',msg:'supprimer'})
            }
        })
    },
    UpdateProf: function (req, res){
       ProfModel.updateOne({_id:req.params.id}, {
           nom:req.body.nom,
           prenom:req.body.prenom,
           email:req.body.email,
           pwd: req.body.pwd,
            Tel: req.body.Tel,

        },function(err) {
            if (err) {
                res.json({state: 'no'})
            } else {
                res.json({state: 'ProfUpdate'})
            }

        })
    }

};










