const adminModel= require('../modules/admin');

module.exports= {

    addAdmin: function (req, res) {
        const admin = new adminModel({
            nom: req.body.nom,
            prenom: req.body.prenom,
            email: req.body.email

        });
        admin.save(function (err) {
            if (err) {
                res.json({state: 'no', msg: 'vous avez un erreur'})
            } else {
                res.json({state: 'ok', msg: 'admin ajouté'})
            }
        })
    },
    getAll: function (req, res){
        adminModel.find({}, function(err,liste){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(liste)
            }
        })
    },
    getById: function (req, res){
        adminModel.findOne({_id:req.params.id},  function(err,liste){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(liste)
            }
        })
    },
    SupprimerAdmin: function (req, res){
        adminModel.remove({_id:req.params.id}, function(err){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json({state :'ok',msg:'supprimer'})
            }
        })
    },
    UpdateAdmin: function (req, res){
        adminModel.updateOne({_id:req.params.id}, {

            nom: req.body.nom,
            prenom: req.body.prenom,
            email: req.body.email
        },function(err) {
            if (err) {
                res.json({state: 'no'})
            } else {
                res.json({state: 'adminUpdate'})
            }

        })
    },
     //login : function (req ,res) {


//}


};





