const etudModel= require('../modules/etudiant');

var fs =require('fs');
const multer=require ('multer');
const upload = multer({dest:__dirname+'/uploads/images'});

module.exports= {

    ajouter: function (req, res) {
        var file = __dirname + '/uploads/' + req.file.originalname;
        fs.readFile(req.file.path, function (err, data) {
            fs.writeFile(file, data, function (err) {
                {
                    if (err) {
                        res.json({msg :'erreur'})
                    }
                    else {
                        const etud = new etudModel({
                            ddN: req.body.ddN,
                            nom: req.body.nom,
                            prenom: req.body.prenom,
                            email: req.body.email,
                            CIN: req.body.CIN,
                            image: req.file.originalname,
                        });
                        etud.save(function (err) {

                            if (err) {
                                console.log(err)

                                res.json({state: 'no', msg: 'vous avez un erreur'})

                            }
                            else {

                                res.json({state: 'ok', msg: 'etudiant crée avec succées '})
                            }


                        })
                    }
                }
            })
        })
    },





    getAll: function(req, res){
        etudModel.find({}, function(err,liste){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(liste)
            }
        })
    },
    getById: function (req, res){
       etudModel.findOne({_id:req.params.id}).populate('role').exec( function(err,liste){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(liste)
            }
        })
    },
    SupprimerEtud: function (req, res){
       etudModel.remove({_id:req.params.id}, function(err){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json({state :'ok',msg:'supprimer'})
            }
        })
    },
    UpdateEtud: function (req, res){
        etudModel.updateOne({_id:req.params.id}, {

            CIN: req.body.CIN,
            ddN: req.body.ddN,
            image: req.body.image

        },function(err) {
            if (err) {
                res.json({state: 'no'})
            } else {
                res.json({state: 'uetudUpdate'})
            }

        })
    }

};










