const mod =require ('../modules/Module');



module.exports = {
    ADD : function(req,res){
        const module = new mod({
            nom :req.body.nom,
            prof: req.body.prof,
        })
        module.save(function(err){

            if(err){
                res.json ({state:'no',msg:'erreur'});

            }
            else{
                res.json({state:'ok',msg :'mod accepté'});
            }
        })
    },
    pushMatier:function(req,res){
       mod.updateOne({_id:req.params.id},{$push : {matiere : req.body.matiere}},function (err) {


           if(err){
               res.json({state :'no',msg:'vous avez un erreur'})
           }
           else{

               console.log({$push : {matiere : req.body.matiere}})

               res.json({state :'ok',msg:'updated'})
           }
       })

    },
    getAll: function (req, res){
        mod.find({}, function(err,liste){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(liste)
            }
        })
    },
    getById: function (req, res){
        mod.findOne({_id:req.params.id}).populate('prof','nom').exec( function(err,liste){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json(liste)
            }
        })
    },
    Supprimermod: function (req, res){
        mod.remove({_id:req.params.id}, function(err){
            if(err){
                res.json({state :'no',msg:'vous avez un erreur'})
            }
            else{
                res.json({state :'ok',msg:'supprimer'})
            }
        })
    },
    Updatemod: function (req, res){
        mod.updateOne({_id:req.params.id}, {
            nom: req.body.nom,

        },function(err) {
            if (err) {
                res.json({state: 'no'})
            } else {
                res.json({state: 'Update'})
            }
        })
    }
};
