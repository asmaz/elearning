const etud  = require('../controller/etudiant_ctr.js');
const router = require('express').Router();
const multer=require ('multer');
const upload = multer({dest:__dirname+'/uploads/images'});

router.post ('/',upload.single('image'), etud.ajouter);
router.get ('/getById/:id', etud.getById);
router.get ('/getAll', etud.getAll);
router.put ('/Updateetud/:id', etud.UpdateEtud);
router.delete ('/Supprimeretud/:id', etud.SupprimerEtud);



module.exports = router ;