const mod_ctr  = require('../controller/module_ctr');


const router = require('express').Router();


router.post('/add',mod_ctr.ADD);
router.put('/pushMat/:id',mod_ctr.pushMatier);
router.get('/getAll',mod_ctr.getAll);
router.get('/getById/:id',mod_ctr.getById);
router.delete('/supp/:id',mod_ctr.Supprimermod);
router.put('/update/:id',mod_ctr.Updatemod);
module.exports = router ;