
const prof  = require('../controller/prof_ctr');
const jwt =require('jsonwebtoken');
const router = require('express').Router();
router.post('/add',prof.addProf);

router.post('/login',prof.login);

router.get ('/getById/:id', prof.getById);
router.get ('/getAll', ValidateUser,prof.getAll);
router.put ('/Updateprof/:id', prof.UpdateProf);
router.delete ('/Supprimerprof/:id', prof.SupprimerProf);

function ValidateUser(req,res, next){
    jwt.verify(req.headers['x_access_token'],req.app.get('secretKey'),function (err,decoded) {
        if(err){
            res.json({status:"error",message:err.message, data:null});
        }else{
            //add user id to request
            req.body.userId=decoded.id ;
            next();
        }

    })

}

module.exports = router ;