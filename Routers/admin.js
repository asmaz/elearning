const admin  = require('../controller/admin_ctr.js');

const router = require('express').Router();


router.post ('/add', admin.addAdmin);
router.get ('/getById/:id', admin.getById);
router.get ('/getAll', admin.getAll);
router.put ('/UpdateAdmin/:id', admin.UpdateAdmin);
router.delete ('/SupprimerAdmin/:id', admin.SupprimerAdmin);



module.exports = router ;