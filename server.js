const express = require('express');
const bodyParser =require('body-parser');
const adminRouter=require('./Routers/admin');
const etud=require('./Routers/edtu');
const mod =require('./Routers/module_router');
const prof=require('./Routers/prof_router');
const db =require('./modules/db');
const app = express() ;
const cors =require('cors')
app.set('secretKey','55');
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());
app.use('/admin',adminRouter);
app.use('/etud',etud);
app.use('/mod',mod);
app.use('/prof',prof);
app.use(cors('*'));


app.listen(5000,function() {
    console.log('server started')
});


