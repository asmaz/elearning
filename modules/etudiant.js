const mongoose=require('mongoose');
const Schema =mongoose.Schema;
const multer=require('multer');
const admin = require('./admin.js');


const etudiantSchema=  admin.discriminator('etudiant',new mongoose.Schema({

    CIN :{
        type:String,
        required:true,
        trim: true,

    },
    ddN :{
        type:String,
        required:true,
        trim: true,

    },
    image :{
       type : String,
        required : true
    },
    groupe:{
        type :Schema.Types.ObjectID,
        ref:'groupe'
    }

}))

module.exports=etudiantSchema;