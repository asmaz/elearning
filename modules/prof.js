const mongoose = require('mongoose');
const Schema=mongoose.Schema;
const admin = require('./admin.js');
const bcrypt = require('bcryptjs');

const ProfSchema =admin.discriminator('prof',new mongoose.Schema({

        pwd: {
            type: String,
            required: true,
            trim: true


        },
        Tel: {
            type: String,
            required: true,
            trim: true
        },
    modules:{
            type :   Schema.Types.ObjectID,
            ref:'modules'
        }



    })
        .pre('save',function(){
                this.pwd=bcrypt.hashSync(this.pwd,10)
            }
        )

);

module.exports = ProfSchema;


