const mongoose = require('mongoose');
const Schema=mongoose.Schema;

const baseOptions = {

    discriminatorKey : 'typeUser',
    collection : 'utilisateurs'
};

const adminSchema = new mongoose.model('admin',new mongoose.Schema({
    nom: {
        type: String,
        required: true,
        trim: true
    },
    prenom: {
        type: String,
        required: true,
        trim: true


    },
    email: {
        type: String,
        required: true,
        trim: true


    },
}

       , baseOptions
)
)

module.exports = adminSchema ;


