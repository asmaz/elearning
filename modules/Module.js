const mongoose=require('mongoose');

const Schema =mongoose.Schema;

const matiere = new  Schema({


   nom :{

       type :String, required: true, trim: true

   },
   nbre_heure :{

       type :String, required: true, trim: true

   }

});

const moduleSchema=mongoose.model('modules',new mongoose.Schema({

    nom :{
        type:String,
        required:true,
        trim: true,

    },


    prof : {
        type : mongoose.Schema.Types.ObjectID,
        ref : 'prof'


    },

    matiere  : [matiere]





}))

module.exports=moduleSchema;